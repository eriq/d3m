## vNEXT

* Fixed a case where automatically generated metadata overrode explicitly set existing metadata.
  [!25](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/25)
* Fixed `query_with_exceptions` metadata method to correctly return exceptions for
  deeper selectors.
* Added to `primitive_family`:
  * `SCHEMA_DISCOVERY`
* Added to `algorithm_types`:
  * `HEURISTIC`
  * `MARKOV_RANDOM_FIELD`
* Added `PrimitiveNotFittedError`, `DimensionalityMismatchError`, and `MissingValueError`
  exceptions to `d3m.exceptions`.
  [!22](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/22)
* Fixed setting semantic types for boundary columns.
  [#126](https://gitlab.com/datadrivendiscovery/d3m/issues/126) [!23](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/23)
* Added `video/avi` media type to lists of known media types.
* Fixed a type check which prevented an additional primitive argument to be of `Union` type.
* Fixed erroneous removal of empty dicts (`{}`) from metadata when empty dicts were
  explicitly stored in metadata.
  [#118](https://gitlab.com/datadrivendiscovery/d3m/issues/118)
* Made sure that conflicting entry points are resolved in a deterministic way.
* Made sure primitive metadata's `python_path` matches the path under which
  a primitive is registered under `d3m.primitives`. This also prevents
  a primitive to be registered twice at different paths in the namespace.
  [#4](https://gitlab.com/datadrivendiscovery/d3m/issues/4)
* Fixed a bug which prevented registration of primitives at deeper levels
  (e.g., `d3m.primitives.<name1>.<name2>.<primitive>`).
  [#121](https://gitlab.com/datadrivendiscovery/d3m/issues/121)

## v2018.6.5

* `Metadata` class got additional methods to manipulate metadata:
    * `remove(selector)` removes metadata at `selector`.
    * `query_with_exceptions(selector)` to return metadata for selectors which
      have metadata which differs from that of `ALL_ELEMENTS`.
    * `add_semantic_type`, `has_semantic_type`, `remove_semantic_type`,
      `get_elements_with_semantic_type` to help with semantic types.
    * `query_column`, `update_column`, `remove_column`, `get_columns_with_semantic_type`
      to make it easier to work with tabular data.

    [#55](https://gitlab.com/datadrivendiscovery/d3m/issues/55)
    [#78](https://gitlab.com/datadrivendiscovery/d3m/issues/78) 

* Container `List` now inherits from a regular Python `list` and not from `typing.List`.
  It does not have anymore a type variable. Typing information is stored in `metadata`
  anyway (`structural_type`). This simplifies type checking (and improves performance)
  and fixes pickling issues.
  **Backwards incompatible.**
* `Hyperparams` class' `defaults` method now accepts optional `path` argument which
  allows one to fetch defaults from nested hyper-parameters.
* `Hyperparameters` class and its subclasses now have `get_default` method instead
  of a property `default`. 
  **Backwards incompatible.**
* `Hyperparams` class got a new method `replace` which makes it easier to modify
  hyper-parameter values.
* `Set` hyper-parameter can now accept also a hyper-parameters configuration as elements
  which allows one to define a set of multiple hyper-parameters per each set element.
  [#94](https://gitlab.com/datadrivendiscovery/d3m/issues/94)
* Pipeline's `check` method now checks structural types of inputs and outputs and assures
  they match.
  [!19](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/19)
* `Set` hyper-parameter now uses tuple of unique elements instead of set to represent the set.
  This assures that the order of elements is preserved to help with reproducibility when
  iterating over a set.
  **Backwards incompatible.**
  [#109](https://gitlab.com/datadrivendiscovery/d3m/issues/109)
* `Set` hyper-parameter can now be defined without `max_samples` argument to allow a set
  without an upper limit on the number of elements.
  `min_samples` and `max_samples` arguments to `Set` constructor have been switched as
  a consequence, to have a more intuitive order.
  Similar changes have been done to `sample_multiple` method of hyper-parameters.
  **Backwards incompatible.**
  [#110](https://gitlab.com/datadrivendiscovery/d3m/issues/110)
* Core dependencies have been upgraded: `numpy==1.14.3`. `pytypes` is now a released version.
* When converting a numpy array with more than 2 dimensions to a DataFrame, higher dimensions are
  automatically converted to nested numpy arrays inside a DataFrame.
  [#80](https://gitlab.com/datadrivendiscovery/d3m/issues/80)
* Metadata is now automatically preserved when converting between container types.
  [#76](https://gitlab.com/datadrivendiscovery/d3m/issues/76)
* Basic metadata for data values is now automatically generated when using D3M container types.
  Value is traversed over its structure and `structural_type` and `dimension` with its `length`
  keys are populated. Some `semantic_types` are added in simple cases, and `dimension`'s
  `name` as well. In some cases analysis of all data to generate metadata can take time,
  so you might consider disabling automatic generation by setting `generate_metadata`
  to `False` in container's constructor or `set_for_value` calls and then manually populating
  necessary metadata.
  [#35](https://gitlab.com/datadrivendiscovery/d3m/issues/35)
  [!6](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/6)
  [!11](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/11)
* When reading D3M datasets, `media_types` metadata now includes proper media types
  for the column, and also media type for each particular row (file).
* D3M dataset and problem description parsing has been updated to 3.1.2 version:
    * `Dataset` class now supports loading `edgeList` resources.
    * `primitive_family` now includes `OBJECT_DETECTION`.
    * `task_type` now includes `OBJECT_DETECTION`.
    * `performance_metrics` now includes `PRECISION`, `RECALL`, `OBJECT_DETECTION_AVERAGE_PRECISION`.
    * `targets` of a problem description now includes `clusters_number`.
    * New metadata `boundary_for` can now describe for which other column
      a column is a boundary for.
    * Support for `realVector`, `json` and `geojson` column types.
    * Support for `boundingBox` column role.
    * New semantic types:
      * `https://metadata.datadrivendiscovery.org/types/EdgeList`
      * `https://metadata.datadrivendiscovery.org/types/FloatVector`
      * `https://metadata.datadrivendiscovery.org/types/JSON`
      * `https://metadata.datadrivendiscovery.org/types/GeoJSON`
      * `https://metadata.datadrivendiscovery.org/types/Interval`
      * `https://metadata.datadrivendiscovery.org/types/IntervalStart`
      * `https://metadata.datadrivendiscovery.org/types/IntervalEnd`
      * `https://metadata.datadrivendiscovery.org/types/BoundingBox`
      * `https://metadata.datadrivendiscovery.org/types/BoundingBoxXMin`
      * `https://metadata.datadrivendiscovery.org/types/BoundingBoxYMin`
      * `https://metadata.datadrivendiscovery.org/types/BoundingBoxXMax`
      * `https://metadata.datadrivendiscovery.org/types/BoundingBoxYMax`
    
    [#99](https://gitlab.com/datadrivendiscovery/d3m/issues/99)
    [#107](https://gitlab.com/datadrivendiscovery/d3m/issues/107)

* Unified the naming of attributes/features metafeatures to attributes.
  **Backwards incompatible.**
  [!13](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/13)
* Unified the naming of categorical/nominal metafeatures to categorical.
  **Backwards incompatible.**
  [!12](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/12)
* Added more metafeatures:
    * `pca`
    * `random_tree`
    * `decision_stump`
    * `naive_bayes`
    * `linear_discriminant_analysis`
    * `knn_1_neighbor`
    * `c45_decision_tree`
    * `rep_tree`
    * `categorical_joint_entropy`
    * `numeric_joint_entropy`
    * `number_distinct_values_of_numeric_features`
    * `class_probabilities`
    * `number_of_features`
    * `number_of_instances`
    * `canonical_correlation`
    * `entropy_of_categorical_features`
    * `entropy_of_numeric_features`
    * `equivalent_number_of_categorical_features`
    * `equivalent_number_of_numeric_features`
    * `mutual_information_of_categorical_features`
    * `mutual_information_of_numeric_features`
    * `categorical_noise_to_signal_ratio`
    * `numeric_noise_to_signal_ratio`

    [!10](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/10) 
    [!14](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/14)
    [!17](https://gitlab.com/datadrivendiscovery/d3m/merge_requests/17)

* Added metafeatures for present values:
    * `number_of_instances_with_present_values`
    * `ratio_of_instances_with_present_values`
    * `number_of_present_values`
    * `ratio_of_present_values`

    [#84](https://gitlab.com/datadrivendiscovery/d3m/issues/84)  
   
* Implemented interface for saving datasets.
  [#31](https://gitlab.com/datadrivendiscovery/d3m/issues/31)
* To remove a key in metadata, instead of using `None` value one should now use
  special `NO_VALUE` value.
  **Backwards incompatible.**
* `None` is now serialized to JSON as `null` instead of string `"None"`.
  **Could be backwards incompatible.**
* Unified naming and behavior of methods dealing with JSON and JSON-related
  data. Now across the package:
    * `to_json_structure` returns a structure with values fully compatible with JSON and serializable with default JSON serializer
    * `to_simple_structure` returns a structure similar to JSON, but with values left as Python values
    * `to_json` returns serialized value as JSON string

    **Backwards incompatible.**

* Hyper-parameters are now required to specify at least one
  semantic type from: `https://metadata.datadrivendiscovery.org/types/TuningParameter`,
  `https://metadata.datadrivendiscovery.org/types/ControlParameter`,
  `https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter`,
  `https://metadata.datadrivendiscovery.org/types/MetafeatureParameter`.
  **Backwards incompatible.**
* Made type strings in primitive annotations deterministic.
  [#93](https://gitlab.com/datadrivendiscovery/d3m/issues/93)
* Reimplemented primitives loading code to load primitives lazily.
  [#74](https://gitlab.com/datadrivendiscovery/d3m/issues/74)
* `d3m.index` module now has new and modified functions:
    * `search` now returns a list of Python paths of all potential
      primitives defined through entry points (but does not load them
      or checks if entry points are valid)
    * `get_primitive` loads and returns a primitive given its Python path
    * `get_primitive_by_id` returns a primitive given its ID, but a primitive
      has to be loaded beforehand
    * `get_loaded_primitives` returns a list of all currently loaded primitives
    * `load_all` tries to load all primitives
    * `register_primitive` now accepts full Python path instead of just suffix

    **Backwards incompatible.**
    [#74](https://gitlab.com/datadrivendiscovery/d3m/issues/74)  

* Defined `model_features` primitive metadata to describe features supported
  by an underlying model. This is useful to allow easy matching between
  problem's subtypes and relevant primitives.
  [#88](https://gitlab.com/datadrivendiscovery/d3m/issues/88)
* Made hyper-parameter space of an existing `Hyperparams` subclass immutable.
  [#91](https://gitlab.com/datadrivendiscovery/d3m/issues/91)
* `d3m.index describe` command now accept `-s`/`--sort-keys` argument which
  makes all keys in the JSON output sorted, making output JSON easier to
  diff and compare.
* `can_accept` now gets a `hyperparams` object with hyper-parameters under
  which to check a method call. This allows `can_accept` to return a result
  based on control hyper-parameters.
  **Backwards incompatible.**
  [#81](https://gitlab.com/datadrivendiscovery/d3m/issues/81)
* Documented that all docstrings should be made according to
  [numpy docstring format](https://numpydoc.readthedocs.io/en/latest/format.html).
  [#85](https://gitlab.com/datadrivendiscovery/d3m/issues/85)
* Added to semantic types:
  * `https://metadata.datadrivendiscovery.org/types/MissingData`
  * `https://metadata.datadrivendiscovery.org/types/InvalidData`
  * `https://metadata.datadrivendiscovery.org/types/RedactedTarget`
  * `https://metadata.datadrivendiscovery.org/types/RedactedPrivilegedData`
* Added to `primitive_family`:
  * `TIME_SERIES_EMBEDDING`
* Added to `algorithm_types`:
  * `IVECTOR_EXTRACTION`
* Removed `SparseDataFrame` from standard container types because it is being
  deprecated in Pandas.
  **Backwards incompatible.**
  [#95](https://gitlab.com/datadrivendiscovery/d3m/issues/95)
* Defined `other_names` metadata field for any other names a value might have.
* Optimized primitives loading time.
  [#87](https://gitlab.com/datadrivendiscovery/d3m/issues/87)
* Made less pickling of values when hyper-parameter has `Union` structural type.
  [#83](https://gitlab.com/datadrivendiscovery/d3m/issues/83)
* `DataMetadata.set_for_value` now first checks new value against the metadata, by default.
  **Could be backwards incompatible.**
* Added `NO_NESTED_VALUES` primitive precondition and effect.
  This allows primitive to specify if it cannot handle values where a container value
  contains nested other values with dimensions.

## v2018.4.18

* Added `pipeline.json` JSON schema to this package. Made `problem.json` JSON schema
  describing parsed problem description's schema. There is also a `d3m.metadata.pipeline`
  parser for pipelines in this schema and Python object to represent a pipeline.
  [#53](https://gitlab.com/datadrivendiscovery/d3m/issues/53)
* Updated README to make it explicit that for tabular data the first dimension
  is always rows and the second always columns, even in the case of a DataFrame
  container type.
  [#54](https://gitlab.com/datadrivendiscovery/d3m/issues/54)
* Made `Dataset` container type return Pandas `DataFrame` instead of numpy `ndarray`
  and in generaly suggest to use Pandas `DataFrame` as a default container type.
  **Backwards incompatible.**
  [#49](https://gitlab.com/datadrivendiscovery/d3m/issues/49)
* Added `UniformBool` hyper-parameter class.
* Renamed `FeaturizationPrimitiveBase` to `FeaturizationLearnerPrimitiveBase`.
  **Backwards incompatible.**
* Defined `ClusteringTransformerPrimitiveBase` and renamed `ClusteringPrimitiveBase`
  to `ClusteringLearnerPrimitiveBase`.
  **Backwards incompatible.**
  [#20](https://gitlab.com/datadrivendiscovery/d3m/issues/20)
* Added `inputs_across_samples` decorator to mark which method arguments
  are inputs which compute across samples.
  [#19](https://gitlab.com/datadrivendiscovery/d3m/issues/19)
* Converted `SingletonOutputMixin` to a `singleton` decorator. This allows
  each produce method separately to be marked as a singleton produce method.
  **Backwards incompatible.**
  [#17](https://gitlab.com/datadrivendiscovery/d3m/issues/17)
* `can_accept` can also raise an exception with information why it cannot accept.
  [#13](https://gitlab.com/datadrivendiscovery/d3m/issues/13)
* Added `Primitive` hyper-parameter to describe a primitive or primitives.
  Additionally, documented in docstrings better how to define hyper-parameters which
  use primitives for their values and how should such primitives-as-values be passed
  to primitives as their hyper-parameters.
  [#51](https://gitlab.com/datadrivendiscovery/d3m/issues/51)
* Hyper-parameter values can now be converted to and from JSON-compatible structure
  using `values_to_json` and `values_from_json` methods. Non-primitive values
  are pickled and stored as base64 strings.
  [#67](https://gitlab.com/datadrivendiscovery/d3m/issues/67)
* Added `Choice` hyper-parameter which allows one to define
  combination of hyper-parameters which should exists together.
  [#28](https://gitlab.com/datadrivendiscovery/d3m/issues/28)
* Added `Set` hyper-parameter which samples multiple times another hyper-parameter.
  [#52](https://gitlab.com/datadrivendiscovery/d3m/issues/52)
* Added `https://metadata.datadrivendiscovery.org/types/MetafeatureParameter`
  semantic type for hyper-parameters which control which meta-features are
  computed by the primitive.
  [#41](https://gitlab.com/datadrivendiscovery/d3m/issues/41)
* Added `supported_media_types` primitive metadata to describe
  which media types a primitive knows how to manipulate.
  [#68](https://gitlab.com/datadrivendiscovery/d3m/issues/68)
* Renamed metadata property `mime_types` to `media_types`.
  **Backwards incompatible.**
* Made pyarrow dependency a package extra. You can depend on it using
  `d3m[arrow]`.
  [#66](https://gitlab.com/datadrivendiscovery/d3m/issues/66)
* Added `multi_produce` method to primitive interface which allows primitives
  to optimize calls to multiple produce methods they might have.
  [#21](https://gitlab.com/datadrivendiscovery/d3m/issues/21)
* Added `d3m.utils.redirect_to_logging` context manager which can help
  redirect primitive's output to stdout and stderr to primitive's logger.
  [#65](https://gitlab.com/datadrivendiscovery/d3m/issues/65)
* Primitives can now have a dependency on static files and directories.
  One can use `FILE` and `TGZ` entries in primitive's `installation`
  metadata to ask the caller to provide paths those files and/or
  extracted directories through new `volumes` constructor argument.
  [#18](https://gitlab.com/datadrivendiscovery/d3m/issues/18)
* Core dependencies have been upgraded: `numpy==1.14.2`, `networkx==2.1`.
* LUPI quality in D3M datasets is now parsed into
  `https://metadata.datadrivendiscovery.org/types/SuggestedPrivilegedData`
  semantic type for a column.
  [#61](https://gitlab.com/datadrivendiscovery/d3m/issues/61)
* Support for primitives using Docker containers has been put on hold.
  We are keeping a way to pass information about running containers to a
  primitive and defining dependent Docker images in metadata, but currently
  it is not expected that any runtime running primitives will run
  Docker containers for a primitive.
  [#18](https://gitlab.com/datadrivendiscovery/d3m/issues/18)
* Primitives do not have to define all constructor arguments anymore.
  This allows them to ignore arguments they do not use, e.g.,
  `docker_containers`.
  On the other side, when creating an instance of a primitive, one
  has now to check which arguments the constructor accepts, which is
  available in primitive's metadata:
  `primitive.metadata.query()['primitive_code'].get('instance_methods', {})['__init__']['arguments']`.
  [#63](https://gitlab.com/datadrivendiscovery/d3m/issues/63)
* Information about running primitive's Docker container has changed
  from just its address to a `DockerContainer` tuple containing both
  the address and a map of all exposed ports.
  At the same time, support for Docker has been put on hold so you
  do not really have to upgrade for this change anything and can simply
  remove the `docker_containers` argument from primitive's constructor.
  **Backwards incompatible.**
  [#14](https://gitlab.com/datadrivendiscovery/d3m/issues/14)
* Multiple exception classes have been defined in `d3m.exceptions`
  module and are now in use. This allows easier and more precise
  handling of exceptions.
  [#12](https://gitlab.com/datadrivendiscovery/d3m/issues/12)
* Fixed inheritance of `Hyperparams` class.
  [#44](https://gitlab.com/datadrivendiscovery/d3m/issues/44)
* Each primitive's class now automatically gets an instance of
  [Python's logging](https://docs.python.org/3/library/logging.html)
  logger stored into its ``logger`` class attribute. The instance is made
  under the name of primitive's ``python_path`` metadata value. Primitives
  can use this logger to log information at various levels (debug, warning,
  error) and even associate extra data with log record using the ``extra``
  argument to the logger calls.
  [#10](https://gitlab.com/datadrivendiscovery/d3m/issues/10)
* Made sure container data types can be serialized with Arrow/Plasma
  while retaining their metadata.
  [#29](https://gitlab.com/datadrivendiscovery/d3m/issues/29)
* `Scores` in `GradientCompositionalityMixin` replaced with `Gradients`.
  `Scores` only makes sense in a probabilistic context.
* Renamed `TIMESERIES_CLASSIFICATION`, `TIMESERIES_FORECASTING`, and
  `TIMESERIES_SEGMENTATION` primitives families to
  `TIME_SERIES_CLASSIFICATION`, `TIME_SERIES_FORECASTING`, and
  `TIME_SERIES_SEGMENTATION`, respectively, to match naming
  pattern used elsewhere.
  Similarly, renamed `UNIFORM_TIMESERIES_SEGMENTATION` algorithm type
  to `UNIFORM_TIME_SERIES_SEGMENTATION`.
  Compound words using hyphens are separated, but hyphens for prefixes
  are not separated. So "Time-series" and "Root-mean-squared error"
  become `TIME_SERIES` and `ROOT_MEAN_SQUARED_ERROR`
  but "Non-overlapping" and "Multi-class" are `NONOVERLAPPING` and `MULTICLASS`.
  **Backwards incompatible.**
* Updated performance metrics to include `PRECISION_AT_TOP_K` metric.
* Added to problem description parsing support for additional metric
  parameters and updated performance metric functions to use them.
  [#42](https://gitlab.com/datadrivendiscovery/d3m/issues/42)
* Merged `d3m_metadata`, `primitive_interfaces` and `d3m` repositories
  into `d3m` repository. This requires the following changes of
  imports in existing code:
    * `d3m_metadata` to `d3m.metadata`
    * `primitive_interfaces` to `d3m.primitive_interfaces`
    * `d3m_metadata.container` to `d3m.container`
    * `d3m_metadata.metadata` to `d3m.metadata.base`
    * `d3m_metadata.metadata.utils` to `d3m.utils`
    * `d3m_metadata.metadata.types` to `d3m.types`

    **Backwards incompatible.**
    [#11](https://gitlab.com/datadrivendiscovery/d3m/issues/11)

* Fixed computation of sampled values for `LogUniform` hyper-parameter class.
  [#47](https://gitlab.com/datadrivendiscovery/d3m/issues/47)
* When copying or slicing container values, metadata is now copied over
  instead of cleared. This makes it easier to propagate metadata.
  This also means one should make sure to update the metadata in the
  new container value to reflect changes to the value itself.
  **Could be backwards incompatible.**
* `DataMetadata` now has `set_for_value` method to make a copy of
  metadata and set new `for_value` value. You can use this when you
  made a new value and you want to copy over metadata, but you also
  want this value to be associated with metadata. This is done by
  default for container values.
* Metadata now includes SHA256 digest for primitives and datasets.
  It is computed automatically during loading. This should allow one to
  track exact version of primitive and datasets used.
  `d3m.container.dataset.get_d3m_dataset_digest` is a reference
  implementation of computing digest for D3M datasets.
  You can set `compute_digest` to `False` to disable this.
  You can set `strict_digest` to `True` to raise an exception instead
  of a warning if computed digest does not match one in metadata.
* Datasets can be now loaded in "lazy" mode: only metadata is loaded
  when creating a `Dataset` object. You can use `is_lazy` method to
  check if dataset iz lazy and data has not yet been loaded. You can use
  `load_lazy` to load data for a lazy object, making it non-lazy.
* There is now an utility metaclass `d3m.metadata.utils.AbstractMetaclass`
  which makes classes which use it automatically inherit docstrings
  for methods from the parent. Primitive base class and some other D3M
  classes are now using it.
* `d3m.metadata.base.CONTAINER_SCHEMA_VERSION` and
  `d3m.metadata.base.DATA_SCHEMA_VERSION` were fixed to point to the
  correct URI.
* Many `data_metafeatures` properties in metadata schema had type
  `numeric` which does not exist in JSON schema. They were fixed to
  `number`.
* Added to a list of known semantic types:
  `https://metadata.datadrivendiscovery.org/types/Target`,
  `https://metadata.datadrivendiscovery.org/types/PredictedTarget`,
  `https://metadata.datadrivendiscovery.org/types/TrueTarget`,
  `https://metadata.datadrivendiscovery.org/types/Score`,
  `https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint`,
  `https://metadata.datadrivendiscovery.org/types/SuggestedPrivilegedData`,
  `https://metadata.datadrivendiscovery.org/types/PrivilegedData`.
* Added to `algorithm_types`: `ARRAY_CONCATENATION`, `ARRAY_SLICING`,
  `ROBUST_PRINCIPAL_COMPONENT_ANALYSIS`, `SUBSPACE_CLUSTERING`,
  `SPECTRAL_CLUSTERING`, `RELATIONAL_ALGEBRA`, `MULTICLASS_CLASSIFICATION`,
  `MULTILABEL_CLASSIFICATION`, `OVERLAPPING_CLUSTERING`, `SOFT_CLUSTERING`,
  `STRICT_PARTITIONING_CLUSTERING`, `STRICT_PARTITIONING_CLUSTERING_WITH_OUTLIERS`,
  `UNIVARIATE_REGRESSION`, `NONOVERLAPPING_COMMUNITY_DETECTION`,
  `OVERLAPPING_COMMUNITY_DETECTION`.

## v2018.1.26

* Test primitives updated to have `location_uris` metadata.
* Test primitives updated to have `#egg=` package URI suffix in metadata.
* Primitives (instances of their classes) can now be directly pickled
  and unpickled. Internally it uses `get_params` and `set_params` in
  default implementation. If you need to preserve additional state consider
  extending `__getstate__` and `__setstate__` methods.
* Added `RandomPrimitive` test primitive.
* Bumped `numpy` dependency to `1.14` and `pandas` to `0.22`.
* Added `https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter` as a known URI
  for `semantic_types` to help convey which hyper-parameters control the use of resources by the
  primitive.
  [#41](https://gitlab.com/datadrivendiscovery/metadata/issues/41)
* Fixed use of `numpy` values in `Params` and `Hyperparams`.
  [#39](https://gitlab.com/datadrivendiscovery/metadata/issues/39)
* Added `upper_inclusive` argument to `UniformInt`, `Uniform`, and `LogUniform` classes
  to signal that the upper bound is inclusive (default is exclusive).
  [#38](https://gitlab.com/datadrivendiscovery/metadata/issues/38)
* Made `semantic_types` and `description` keyword-only arguments in hyper-parameter description classes.
* Made all enumeration metadata classes have their instances be equal to their string names.
* Made sure `Hyperparams` subclasses can be pickled and unpickled.
* Improved error messages during metadata validation.
* Documented common metadata for primitives and data in the README.
* Added standard deviation to aggregate metadata values possible.
* Added `NO_JAGGED_VALUES` to `preconditions` and `effects`.
* Added to `algorithm_types`: `AGGREGATE_FUNCTION`, `AUDIO_STREAM_MANIPULATION`, `BACKWARD_DIFFERENCE_CODING`,
  `BAYESIAN_LINEAR_REGRESSION`, `CATEGORY_ENCODER`, `CROSS_VALIDATION`, `DISCRETIZATION`, `ENCODE_BINARY`,
  `ENCODE_ORDINAL`, `FEATURE_SCALING`, `FORWARD_DIFFERENCE_CODING`, `FREQUENCY_TRANSFORM`, `GAUSSIAN_PROCESS`,
  `HASHING`, `HELMERT_CODING`, `HOLDOUT`, `K_FOLD`, `LEAVE_ONE_OUT`, `MERSENNE_TWISTER`, `ORTHOGONAL_POLYNOMIAL_CODING`,
  `PASSIVE_AGGRESSIVE`, `PROBABILISTIC_DATA_CLEANING`, `QUADRATIC_DISCRIMINANT_ANALYSIS`, `RECEIVER_OPERATING_CHARACTERISTIC`,
  `RELATIONAL_DATA_MINING`, `REVERSE_HELMERT_CODING`, `SEMIDEFINITE_EMBEDDING`, `SIGNAL_ENERGY`, `SOFTMAX_FUNCTION`,
  `SPRUCE`, `STOCHASTIC_GRADIENT_DESCENT`, `SUM_CODING`, `TRUNCATED_NORMAL_DISTRIBUTION`, `UNIFORM_DISTRIBUTION`.
* Added to `primitive_family`: `DATA_GENERATION`, `DATA_VALIDATION`, `DATA_WRANGLING`, `VIDEO_PROCESSING`.
* Added `NoneType` to the list of data types allowed inside container types.
* For `PIP` dependencies specified by a `package_uri` git URI, an `#egg=package_name` URI suffix is
  now required.

## v2018.1.5

* Made use of the PyPI package official. Documented a requirement for
  `--process-dependency-links` argument during installation.
  [#27](https://gitlab.com/datadrivendiscovery/metadata/issues/27)
* Arguments `learning_rate` and `weight_decay` in `GradientCompositionalityMixin` renamed to
  `fine_tune_learning_rate` and `fine_tune_weight_decay`, respectively.
  `learning_rate` is a common hyper-parameter name.
  [#41](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/41)
* Added `https://metadata.datadrivendiscovery.org/types/TuningParameter` and
  `https://metadata.datadrivendiscovery.org/types/ControlParameter` as two known URIs for
  `semantic_types` to help convey which hyper-parameters are true tuning parameters (should be
  tuned during hyper-parameter optimization phase) and which are control parameters (should be
  determined during pipeline construction phase and are part of the logic of the pipeline).
* Made `installation` metadata optional. This allows local-only primitives.
  You can still register them into D3M namespace using `d3m.index.register_primitive`.
* Fixed serialization to JSON of hyper-parameters with `q` argument.
* Clarified that primitive's `PIP` dependency `package` has to be installed with `--process-dependency-link` argument
  enabled, and `package_uri` with both `--process-dependency-link` and `--editable`, so that primitives can have access
  to their git history to generate metadata.
* Only `git+http` and `git+https` URI schemes are allowed for git repository URIs for `package_uri`.
* Added to `algorithm_types`: `AUDIO_MIXING`, `CANONICAL_CORRELATION_ANALYSIS`, `DATA_PROFILING`, `DEEP_FEATURE_SYNTHESIS`,
  `INFORMATION_ENTROPY`, `MFCC_FEATURE_EXTRACTION`, `MULTINOMIAL_NAIVE_BAYES`, `MUTUAL_INFORMATION`, `PARAMETRIC_TRAJECTORY_MODELING`,
  `SIGNAL_DITHERING`, `SIGNAL_TO_NOISE_RATIO`, `STATISTICAL_MOMENT_ANALYSIS`, `UNIFORM_TIMESERIES_SEGMENTATION`.
* Added to `primitive_family`: `SIMILARITY_MODELING`, `TIMESERIES_CLASSIFICATION`, `TIMESERIES_SEGMENTATION`.

## v2017.12.27

* Documented `produce` method for `ClusteringPrimitiveBase` and added
  `ClusteringDistanceMatrixMixin`.
  [#18](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/18)
* Added `can_accept` class method to primitive base class and implemented its
  default implementation.
  [#20](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/20)
* "Distance" primitives now accept an extra argument instead of a tuple.
* `Params` should now be a subclass of `d3m.metadata.params.Params`, which is a
  specialized dict instead of a named tuple.
* Removed `Graph` class. There is no need for it anymore because we can identify
  them by having input type a NetworkX graph and through metadata discovery.
* Added `timeout` and `iterations` arguments to more methods.
* Added `forward` and `backward` backprop methods to `GradientCompositionalityMixin`
  to allow end-to-end backpropagation across diverse primitives.
  [#26](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/26)
* Added `log_likelihoods` method to `ProbabilisticCompositionalityMixin`.
* Constructor now accepts `docker_containers` argument with addresses of running
  primitive's Docker containers.
  [#25](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/25)
* Removed `CallMetadata` and `get_call_metadata` and changed so that some methods
  directly return new but similar `CallResult`.
  [#27](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/27)
* Documented how extra arguments to standard and extra methods can be defined.
* Documented that all arguments with the same name in all methods should have the
  same type. Arguments are per primitive not per method.
  [#29](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/29)
* Specified how to define extra "produce" methods which have same semantics
  as `produce` but different output types.
  [#30](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/30)
* Added `SingletonOutputMixin` to signal that primitive's output contains
  only one element.
  [#15](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/15)
* Added `get_loss_primitive` to allow accessing to the loss primitive
  being used.
* Moved `set_training_data` back to the base class.
  This breaks Liskov substitution principle.
  [#19](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/19)
* Renamed `__metadata__` to `metadata` attribute.
  [#23](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/23)
* `set_random_seed` method has been removed and replaced with a
  `random_seed` argument to the constructor, which is also exposed as an attribute.
  [#16](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/16)
* Primitives have now `hyperparams` attribute which returns a
  hyper-parameters object passed to the constructor.
  [#14](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/14)
* `Params` and `Hyperparams` are now required to be pickable and copyable.
  [#3](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/3)
* Primitives are now parametrized by `Hyperparams` type variable as well.
  Constructor now receives hyper-parameters as an instance as one argument
  instead of multiple keyword arguments.
  [#13](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/13)
* `LossFunctionMixin`'s `get_loss_function` method now returns a value from
  problem schema `Metric` enumeration.
* `LossFunctionMixin` has now a `loss` and `losses` methods which allows one
  to ask a primitive to compute loss for a given set of inputs and outputs using
  internal loss function the primitive is using.
  [#17](https://gitlab.com/datadrivendiscovery/primitive-interfaces/issues/17)
* Added `Params` class.
* Removed `Graph` class in favor of NetworkX `Graph` class.
* Added `Metadata` class with subclasses and documented the use of selectors.
* Added `Hyperparams` class.
* Added `Dataset` class.
* "Sequences" have generally been renamed to "containers". Related code is also now under
  `d3m.container` and not under `d3m.metadata.sequence` anymore.
* `__metadata__` attribute was renamed to `metadata`.
* Package renamed from `d3m_types` to `d3m_metadata`.
* Added schemas for metadata contexts.
* A problem schema parsing and Python enumerations added in
  `d3m.metadata.problem` module.
* A standard set of container and base types have been defined.
* `d3m.index` command tool rewritten to support three commands: `search`, `discover`,
  and `describe`. See details by running `python -m d3m.index -h`.
* Package now requires Python 3.6.
* Repository migrated to gitlab.com and made public.

## v2017.10.10

* Made `d3m.index` module with API to register primitives into a `d3m.primitives` module
  and searches over it.
* `d3m.index` is also a command-line tool to list available primitives and automatically
  generate JSON annotations for primitives.
* Created `d3m.primitives` module which automatically populates itself with primitives
  using Python entry points.
